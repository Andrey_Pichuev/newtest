﻿using System;
using UnityEngine;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using Game.Character.CharacterController;
using Game.Items;
using Game.Shop;
using UnityEngine.UI;

public class PowerShield : HitEntity
{
	[Separator("Power Shield parameters")]
	
	public static bool powerShieldIsActive = false;
	public static int ID;
	private static float currentProportionOfHealth = 1;

	private bool isPlayer;
	
	//private static int[] hitInfoId = new[] { Shader.PropertyToID("_WorldHitPoint0"), Shader.PropertyToID("_WorldHitPoint1"), Shader.PropertyToID("_WorldHitPoint2") };
	//private static int[] hitTimeId = new[] { Shader.PropertyToID("_HitTime0"), Shader.PropertyToID("_HitTime1"), Shader.PropertyToID("_HitTime2") };
	
	//private Material material;
	private MeshRenderer meshRenderer;
	int lastHit = 0;

	
	/// <summary>
	///   <para>This float lerping between 0 and 1. 0 - robot form, 1 - vehicle form.</para>
	/// </summary>
	private float shieldCondition;
	private float timeToTransformation;
	private float lerpTimer;
	[SerializeField] private Vector3 posForRobotForm;
	[SerializeField] private Vector3 scaleForRobotForm;
	[SerializeField] private Vector3 posForVehicleForm;
	[SerializeField] private Vector3 scaleForVehicleForm;
	private bool lerpInProgress;
	private bool toVehicleForm;

	private void Start()
	{
		MainCollider = GetComponent<Collider>();
		meshRenderer = GetComponent<MeshRenderer>();

		gameObject.layer = LayerMask.NameToLayer("Character");
		SetActivePowerShield(powerShieldIsActive);

		if (GetComponentInParent<Player>() != null)
			isPlayer = true;
		else
			isPlayer = false;
		
		HitEffect = HitEffectType.Sparks;

	}

	public void SetUp()
	{
		Health.Current = Health.Max * currentProportionOfHealth;
		SetUpValueDisplay(Health.Max,Health.Current);

		if (powerShieldIsActive && Health.Current <= 0)
		{
			SetActivePowerShield(false);
			if (isPlayer)
			{
				ShopManager.Instance.Equip(ItemsManager.Instance.GetItem(ID));
				ShopManager.Instance.DeleteFromBI(ID);
			}

		}
	}
	
	public void Transformation(bool toVehicle, float time)
	{
		lerpInProgress = true;
		timeToTransformation = time;
		toVehicleForm = toVehicle;
	}

	public void RestToRobotForm()
	{
		shieldCondition = 0;
		SetShieldCondition();
	}
	
	public void SetShieldCondition()
	{
		SetShieldCondition(shieldCondition);
	}

	public void SetShieldCondition(float condition)
	{
		condition = Mathf.Clamp(condition, 0, 1);
		
		gameObject.transform.localPosition = Vector3.Lerp(posForRobotForm, posForVehicleForm, condition);
		gameObject.transform.localScale = Vector3.Lerp(scaleForRobotForm, scaleForVehicleForm, condition);
	}

	public void SetPosAndSCaleForRobot()
	{
		SetPosAndSCaleForRobot(gameObject.transform.localPosition, gameObject.transform.localScale);
	}

	public void SetPosAndSCaleForRobot(Vector3 pos, Vector3 scale)
	{
		posForRobotForm = pos;
		scaleForRobotForm = scale;
	}
	
	public void SetPosAndSCaleForVehicle()
	{
		SetPosAndSCaleForVehicle(gameObject.transform.localPosition, gameObject.transform.localScale);
	}

	public void SetPosAndSCaleForVehicle(Vector3 pos, Vector3 scale)
	{
		posForVehicleForm = pos;
		scaleForVehicleForm = scale;
	}

	public void OnOwnerDie()
	{
		if(!powerShieldIsActive)
			return;
		
		OnDie();
		SetShieldCondition(0);
	}

	public void SetUpValueDisplay(float max, float current)
	{
		if (Health.StatDisplay != null)
			Health.StatDisplay.Setup(max,current);
	}	

	public void SetActivePowerShield(bool f)
	{
		MainCollider.enabled = f;

		if (!meshRenderer)
			GetComponent<MeshRenderer>().enabled = f;
		else
		{
			meshRenderer.enabled = f;
		}

		powerShieldIsActive = f;
		
		if (Health.StatDisplay != null)
			Health.StatDisplay.gameObject.SetActive(f);
	}
	
	public static void OnBuy()
	{
		currentProportionOfHealth = 1;
	}
	
	public override void OnHit(DamageType damageType, HitEntity owner, float damage, Vector3 hitPos, Vector3 hitVector, float defenceReduction = 0)
	{
		base.OnHit(damageType, owner, damage, hitPos, hitVector, defenceReduction);

		currentProportionOfHealth = Health.Current / Health.Max;
	}

	protected override void OnDie()
	{
		currentProportionOfHealth = 0;
		SetUp();
	}


	private void OnEnable()
	{
		SetUp();
	}


	private void Update()
	{
		if (!lerpInProgress)
			return;
		
		float tic = Time.deltaTime / timeToTransformation;

		shieldCondition += toVehicleForm ? tic : -tic;
			
		if(shieldCondition < 0 || shieldCondition > 1)
			EndLerp();
			
		SetShieldCondition();
	}

	private void EndLerp()
	{
		lerpInProgress = false;
		shieldCondition = toVehicleForm ? 1 : 0;
	}
	
}
