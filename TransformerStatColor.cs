﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace UI.Prefabs.Statistics
{
    [CreateAssetMenu(fileName = "TransformerColor", menuName = "Statistics/TransformerColor", order = 1)]
    [System.Serializable]
    public class TransformerStatColor : ScriptableObject
    {
        [System.Serializable]
        public class ColorList
        {
            public TransformerType type;
            public Color color;
        }
        public List<ColorList> Transformers = new List<ColorList>();
        
        [InspectorButton("SaveColors")]
        public bool saveColors = true;

        public void SaveColors()
        {
            for (int i = 0; i < Transformers.Count; i++)
            {
                SaveColor(Transformers[i].type,Transformers[i].color); 
                Debug.Log("Colors Save");
            }

        }
        
        public void SaveColor(TransformerType type, Color color)
        {
            BaseProfile.ClearValue(type+"r");
            BaseProfile.ClearValue(type+"g");
            BaseProfile.ClearValue(type+"b");
            
            BaseProfile.StoreValue(color.r,type+"r");
            BaseProfile.StoreValue(color.g,type+"g");
            BaseProfile.StoreValue(color.b,type+"b");
        }

        private void OnEnable()
        {
            SaveColors();
        }
    }
}

